import React, { Component } from 'react';
import './App.css';
import PropTypes from 'prop-types';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //Questions will be retrieved from json file
      questions: [],
      answers: [],
      currentQuestion: 0, //Keeps track about which question is currently shown
      score: 0,
      isLoading: true //Used for showing to player and program that data still needs to be fetched
    };
  }

  /*
  fetch for JSON is called in componentDidMount, which happens after component has been mounted and rendered once.
  */
  componentDidMount() {
    fetch('/questions.json', {
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
    }).then((response) => {
      return response.json();
    }).then((json) => {
      this.setState({ questions: json, isLoading: false });
    });
  }

  //handleClick adds answer to answers array and moves to next question index.
  handleClick(i) {
    let answer = this.state.answers;
    let current = this.state.currentQuestion;

    answer.push(i);
    current += 1;

    this.setState({
      answers: answer,
      currentQuestion: current
    });
  }

  /*
  If we are still waiting for data, we will display "Waiting for data" to user. Otherwise we will render the game itself.
  This could be replaced with a loading throbber.
  */
  render() {
    if(this.state.isLoading){
      return(
        <div>Waiting for data</div>
      )
    }

    //We show the question, answer options and current result.
    return (
      <div className="app">
        <h1>Small react based quiz game</h1>
        <ul>
          <Question currentQuestion={this.state.currentQuestion} questions={this.state.questions} />
          <Options currentQuestion={this.state.currentQuestion} questions={this.state.questions} answers={this.state.answers} handleClick={i => this.handleClick(i)} />
        </ul>
        <Result currentQuestion={this.state.currentQuestion} questions={this.state.questions} answers={this.state.answers} />
      </div>
    );
  }
}

//Question shows the current question text.
class Question extends Component {
  //propTypes validates incoming data. Improving reusability by warning about mistakes.
  static get propTypes() {
    return{
      currentQuestion: PropTypes.number,
      questions: PropTypes.array
    }
  }

  render() {
    let questionText;

    if (this.props.currentQuestion < this.props.questions.length) {
      questionText = this.props.questions[this.props.currentQuestion][0];
    } else {
      questionText = 'The game has ended';
    }

    return (
      <div className="question">{questionText}</div>
    );
  }
}

//Option fetches all of the answer options for current question and displays them for player as clickable buttons.
class Options extends Component {
  static get propTypes() {
    return{
      currentQuestion: PropTypes.number,
      questions: PropTypes.array,
      handleClick: PropTypes.func
    }
  }

  render() {
    let options;
    let questions = this.props.questions;
    let currentQuestion = this.props.currentQuestion;

    if (currentQuestion < questions.length) {
      options = questions[currentQuestion][2].map((option, index) => {
        return (
          <li key={option}>
            <button onClick={() => this.props.handleClick(index)}>{option}</button>
          </li>
        );
      }
      );
    }

    return (
      <div className="options"><ul>{options}</ul></div>
    );
  }
}

//Result will show if last question was correct and how many correct answers player has had.
class Result extends Component {
  static get propTypes() {
    return{
      currentQuestion: PropTypes.number,
      questions: PropTypes.array,
      answers: PropTypes.array
    }
  }

  render() {
    let current = 0;
    let score;
    let correct = '';

    var i = 0;
    while (i < this.props.currentQuestion) {
      if (this.props.questions[i][1] === this.props.answers[i]) {
        current += 1;
        correct = 'Correct! ';
      } else {
        correct = 'Wrong! ';
      }
      i += 1;
    }

    score = correct + 'Your score is ' + current + '/' + this.props.currentQuestion;

    return (
      <div className="score">{score}</div>
    );
  }
}

export default App;
